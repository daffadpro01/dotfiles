#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls -la  --color=auto'
PS1='[\u@\h \W]\$ '

neofetch
eval "$(starship init bash)"

