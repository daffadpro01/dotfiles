set -U fish_greeting ""
alias ls='ls -lA --color=auto'
neofetch
starship init fish | source
