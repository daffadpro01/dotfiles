#IMPORTS
import os
import re
import socket
import subprocess
from libqtile import qtile
from libqtile.config import Click, Drag, Group, KeyChord, Key, Match, Screen
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from typing import List

#VARIABLES
mod = "mod4"
terminal = guess_terminal()
browser = "firefox"
filemanager= "thunar"
#launcher = "bash /home/daffad/.config/rofi/launchers/misc/launcher.sh"
launcher = "dmenu_run -c"

#AUTOSTART
@hook.subscribe.startup
def autostart():
        home = os.path.expanduser('~/.config/qtile/autostart.sh')
        subprocess.call([home])

#KEYBINDS
keys = [

    Key([mod], "Return", lazy.spawn(terminal), desc="Launch Terminal"),
    Key([mod], "w", lazy.spawn(browser), desc="Launch Browser"),
    Key([mod, "shift"], "f", lazy.spawn(filemanager), desc="Launch File Manager"),
    Key([mod], "d", lazy.spawn(launcher), desc="Launch Run Launcher"),
    Key([mod], "p", lazy.spawn("bash /home/daffad/.config/rofi/powermenu/powermenu.sh"), desc="Launch Powermenu"),
    Key([mod, "shift"], "a", lazy.spawn("flameshot gui"), desc="Screenshot"),

    Key([], "XF86AudioMute", lazy.spawn("amixer set Master toggle")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer set Master 5%- unmute")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer set Master 5%+ unmute")),

    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(),
        desc="Move window focus to other window"),

    Key([mod, "shift"], "h", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    Key([mod, "shift"], "Left", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "shift"], "Right", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "shift"], "Down", lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "shift"], "Up", lazy.layout.shuffle_up(), desc="Move window up"),


    Key([mod], "t", lazy.window.toggle_floating(), desc="Toggle floating window"),   
    Key([mod], "f", lazy.window.toggle_fullscreen(), desc="Toggle Fulscreen window"),

    Key([mod, "control"], "h", lazy.layout.grow_left(),
        desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(),
        desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(),
        desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),

    Key([mod], "space", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod, "shift"], "c", lazy.window.kill(), desc="Kill focused window"),

    Key([mod], "q", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "shift"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
]

def init_group_names():
       return  [
               ("WWW", {'layout': 'monadtall'}),
               ("DEV", {'layout': 'monadtall'}),
               ("SYS", {'layout': 'monadtall'}),
               ("DOC", {'layout': 'monadtall'}),
               ("VBOX", {'layout': 'monadtall'}),
               ("CHAT", {'layout': 'monadtall'}),
               ("MUS", {'layout': 'monadtall'}),
               ("VID", {'layout': 'monadtall'}),
               ("GFX", {'layout': 'monadtall'})
        ]

def init_groups():
        return [Group(name, **kwargs) for name, kwargs in group_names]

if __name__ in ["config", "__main__"]:
        group_names = init_group_names()
        groups = init_groups()
        
for i, (name, kwargs) in enumerate(group_names, 1):
    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))        # Switch to another group
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name))) # Send current window to another group

layout_theme = {"border_width": 3,
                "margin": 70,
                "border_focus": "#44475a",
                "border_normal": "#1D2330"
                }

layouts = [
    #layout.Columns(border_focus_stack='#d75f5f'),
    layout.MonadTall(
                name = 'XMonad',
                **layout_theme),
    # Try more layouts by unleashing below layouts.
    layout.Stack(**layout_theme),
   # layout.Bsp(
   #             name = 'BSPWM',
   #             **layout_theme),
   # layout.Tile(**layout_theme),
    layout.Max(),
    layout.Floating(**layout_theme),
    # layout.Matrix(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

colors = [["#21252d", "#21252d"], # bg color 
          ["#44475a", "#44475a"], # 
          ["#ffffff", "#ffffff"], # white 
          ["#ff5555", "#ff5555"], # red
          ["#ffb86c", "#ffb86c"], # orange
          ["#4f76c7", "#4f76c7"], # blue
          ["#74438f", "#74438f"], # purple
          ["#ff79c6", "#ff79c6"], # pink
          ["#a3be8c", "#a3be8c"]] # yellow
          

widget_defaults = dict(
    font='Ubuntu',
    fontsize=15,
    padding=3,
    foreground= colors[2],
    background= colors[0],
)

icon_defaults = {
        "padding":5,       
                }
extension_defaults = widget_defaults.copy()

sep = " | "

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.TextBox(' '),
                widget.GroupBox(                       
                       active = colors[2],
                       inactive = colors[2],
                       rounded = False,
                       highlight_color = colors[1],
                       highlight_method = "line",
                       this_current_screen_border = colors[1],
                       this_screen_border = colors [4],
                       other_current_screen_border = colors[1],
                       other_screen_border = colors[4],
                       foreground = colors[2],
                       background = colors[0]
                                ),
                widget.TextBox(sep),
                widget.CurrentLayout(),
                widget.TextBox(sep),
                widget.WindowName(
                        max_chars = 70,
                                 ),
                widget.TextBox(
                        text='',
                        fontsize=30,
                        background=colors[5],
                        **icon_defaults
                              ),
                widget.Net(
                        interface="wlan0",
                        background=colors[0],
                        format = ' {down} ↓↑ {up}',
                          ),
                widget.TextBox(
                        text='',
                        fontsize=25,
                        background=colors[3],
                        **icon_defaults
                              ),
                widget.CPU(
                        format = ' {load_percent}%',
                        core = 'all',
                        background=colors[0],
                        ),
                widget.TextBox(
                        text='',
                        fontsize=30,
                        background=colors[5],
                        **icon_defaults
                              ),
                widget.Memory(
                        format = ' {MemUsed: .0f}{mm}/{MemTotal: .0f}{mm}',
                        background=colors[0],
                        ),
                widget.TextBox(
                        text='',
                        fontsize=30,
                        background=colors[3],
                              ),
                widget.Battery(
                        charge_char='+', 
                        discharge_char='-', 
                        error_message='error',
                        format='{percent:2.0%} ({char})', 
                        battery_name = 'BAT0',
                        background=colors[0]
                        ),
                widget.TextBox(
                        text=' ',
                        background=colors[0]
                              ),        
                widget.TextBox(text=' ', background=colors[0]),
                widget.TextBox(
                              text='',
                              fontsize=15,
                              background=colors[5],
                              ),
                widget.Clock(format='%Y-%m-%d %a %I:%M:%S ',
                              background=colors[0],
                            ),
                widget.Systray(background=colors[0],),
                widget.TextBox(' '),
            ],
            31,
           opacity = 0.8,
           margin = 15,
        ),
    ),
]


# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
        start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
     Match(wm_class='confirmreset'),  # gitk
     Match(wm_class='makebranch'),  # gitk
     Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
])
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're aworking one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
