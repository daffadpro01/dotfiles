-- IMPORTS
import XMonad

import XMonad.Util.EZConfig
import XMonad.Util.SpawnOnce
import XMonad.Util.Run

import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks

import XMonad.Layout.Spacing
import XMonad.Layout.ShowWName

import XMonad.Layout.Tabbed
import XMonad.Layout.BinarySpacePartition

import Graphics.X11.ExtraTypes.XF86

import Data.Monoid
import System.Exit

import XMonad.StackSet
import Data.Map

-- VARIABLES

myTerminal :: String
myTerminal      = "alacritty" --terminal emulator

myBrowser :: String
myBrowser       = "qutebrowser"

myLauncher :: String
myLauncher      = "bash /home/daffad/.config/rofi/launchers/misc/launcher.sh"

myFileManager :: String
myFileManager   = "pcmanfm"

myLockScreen :: String
myLockScreen    = "betterlockscreen -l"

myFocusFollowsMouse :: Bool --if moving mouse moves focus
myFocusFollowsMouse = True

myClickJustFocuses :: Bool
myClickJustFocuses = False

-- workspace names
myWorkspaces    = ["<fn=3> </fn> <fn=1>\61612</fn> <fn=3> </fn>"
		  ,"<fn=3> </fn> <fn=1>\61728</fn> <fn=3> </fn>"
		  ,"<fn=3> </fn> <fn=1>\61729</fn> <fn=3> </fn>"
		  ,"<fn=3> </fn> <fn=1>\63106</fn> <fn=3> </fn>"
		  ,"<fn=3> </fn> <fn=1>\61705</fn> <fn=3> </fn>"
		  ,"<fn=3> </fn> <fn=1>\61508</fn> <fn=3> </fn>"
		  ,"<fn=3> </fn> <fn=1>\61563</fn> <fn=3> </fn>"
		  ,"<fn=3> </fn> <fn=1>\61501</fn> <fn=3> </fn>"
		  ,"<fn=3> </fn> <fn=1>\62786</fn> <fn=3> </fn>"]

myNormalBorderColor  = "#282a36" --unfocused border color
myFocusedBorderColor = "#44475a"  --focused border color
myBorderWidth        = 5       --border length
------------------------------------------------------------------------
-- KEYBINDS
myModMask = mod4Mask --mod key

myKeys conf@(XConfig {XMonad.modMask = modm}) = Data.Map.fromList $

    -- launch a terminal
    [ ((modm .|. shiftMask, xK_Return), spawn myTerminal)

    -- application keybinds
    , ((modm, xK_w), spawn myBrowser)
    , ((modm, xK_f), spawn myFileManager)
    , ((modm, xK_d), spawn myLauncher)
    , ((modm, xK_p), spawn "bash /home/daffad/.config/rofi/powermenu/powermenu.sh")
    , ((modm .|. shiftMask ,xK_l), spawn myLockScreen)
    , ((modm, xK_e), spawn "bash ~/.config/eww/launch_eww")
    , ((modm .|. shiftMask ,xK_a), spawn "flameshot screen")

    , ((modm .|. shiftMask ,xK_Escape), spawn (myTerminal ++ " -e htop"))

    --volume && brightness
    , ((0, xF86XK_AudioMute), spawn "amixer set Master toggle")
    , ((0, xF86XK_AudioLowerVolume), spawn "amixer set Master 5%- unmute")
    , ((0, xF86XK_AudioRaiseVolume), spawn "amixer set Master 5%+ unmute")
    , ((0, xF86XK_MonBrightnessUp),   spawn "brightnessctl set +10%")
    , ((0, xF86XK_MonBrightnessDown), spawn "brightnessctl set 10%-")

    -- close focused window
    , ((modm .|. shiftMask, xK_c), kill)

     -- change layout
    , ((modm,               xK_space), sendMessage NextLayout)

    -- refresh
    , ((modm,               xK_n), refresh)

    -- move focus
    , ((modm,               xK_j), windows XMonad.StackSet.focusDown)
    , ((modm,               xK_k), windows XMonad.StackSet.focusUp)

    -- swap windows
    , ((modm,               xK_Return), windows XMonad.StackSet.swapMaster)
    , ((modm .|. shiftMask, xK_j), windows XMonad.StackSet.swapDown)
    , ((modm .|. shiftMask, xK_k), windows XMonad.StackSet.swapUp)

    -- shrink/expand windows
    , ((modm,               xK_h), sendMessage Shrink)
    , ((modm,               xK_l), sendMessage Expand)

    -- make focused window tiled
    , ((modm,               xK_t), withFocused $ windows . XMonad.StackSet.sink)

    -- quit xmonad (just use mod+p)
    , ((modm .|. shiftMask, xK_q), io (exitWith ExitSuccess))

    -- restart xmonad
    , ((modm,               xK_q), spawn "xmonad --recompile; xmonad --restart")

    ]
    ++
    -- mod-[1..9], Switch to workspace N
    -- mod-shift-[1..9], Move client to workspace N
    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
        , (f, m) <- [(XMonad.StackSet.greedyView, 0), (XMonad.StackSet.shift, shiftMask)]]
  --  ++
    --
    -- mod-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
    -- mod-shift-{w,e,r}, Move client to screen 1, 2, or 3
    --
--    [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
--        | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
--        , (f, m) <- [(XMonad.StackSet.view, 0), (XMonad.StackSet.shift, shiftMask)]]
---------------------------------------------------------------------

-- LAYOUT

myLayout = avoidStruts (tiled ||| emptyBSP ||| Mirror tiled ||| Full)
  where
     -- default tiling algorithm partitions the screen into two panes
     tiled   = 
              Tall nmaster delta ratio 

     -- The default number of windows in the master pane
     nmaster = 1

     -- Default proportion of screen occupied by master pane
     ratio   = 1/2

     -- Percent of screen to increment by when resizing panes
     delta   = 3/100

------------------------------------------------------------------------

-- MANAGE HOOK

myManageHook = composeAll
    [ className =? "MPlayer"        --> doFloat
    , className =? "Gimp"           --> doFloat
    , resource  =? "desktop_window" --> doIgnore
    , resource  =? "kdesktop"       --> doIgnore ]

------------------------------------------------------------------------
myEventHook = mempty
------------------------------------------------------------------------
myLogHook = return ()
------------------------------------------------------------------------
myStartupHook = do
	spawnOnce "nitrogen --restore &"
	spawnOnce "picom &"
        spawnOnce "eww daemon &"
        spawnOnce "doas timedatectl set-ntp true &"
------------------------------------------------------------------------
myBar = "xmobar"

myPP = xmobarPP { 
	        ppCurrent = xmobarColor "#50fa7b" "" . wrap "[" "]" 
              , ppVisible = xmobarColor "#bd93f9" "" . wrap "" "*"
              , ppHidden = xmobarColor "#bd93f9" "" . wrap "" "*"
              , ppHiddenNoWindows = xmobarColor "#bd93f9" ""
              , ppTitle = xmobarColor "#f8f8f2" "" . shorten 60
              , ppSep =  "<fn=2> | </fn>"
              , ppUrgent = xmobarColor "#C45500" "" . wrap "!" "!"  
	        }

toggleStrutsKey XConfig {XMonad.modMask = modMask} = (modMask, xK_b)

main = xmonad =<< statusBar myBar myPP toggleStrutsKey defaults 

-- change variables here
defaults = def {
      -- simple stuff
        XMonad.terminal           = myTerminal,
        XMonad.focusFollowsMouse  = myFocusFollowsMouse,
        XMonad.clickJustFocuses   = myClickJustFocuses,
        XMonad.borderWidth        = myBorderWidth,
        XMonad.modMask            = myModMask,
        XMonad.workspaces         = myWorkspaces,
        XMonad.normalBorderColor  = myNormalBorderColor,
        XMonad.focusedBorderColor = myFocusedBorderColor,

      -- key bindings
        XMonad.keys               = myKeys,
      -- hooks, layouts
        XMonad.layoutHook         = myLayout,
        XMonad.manageHook         = myManageHook,
        XMonad.handleEventHook    = myEventHook,
        XMonad.logHook            = myLogHook,
        XMonad.startupHook        = myStartupHook
    } 

-- | Finally, a copy of the default bindings in simple textual tabular format.
help :: String
help = unlines ["The default modifier key is 'alt'. Default keybindings:",
    "",
    "-- launching and killing programs",
    "mod-Shift-Enter  Launch xterminal",
    "mod-p            Launch dmenu",
    "mod-Shift-c      Close/kill the focused window",
    "mod-Space        Rotate through the available layout algorithms",
    "mod-Shift-Space  Reset the layouts on the current workSpace to default",
    "mod-n            Resize/refresh viewed windows to the correct size",
    "",
    "-- move focus up or down the window stack",
    "mod-Tab        Move focus to the next window",
    "mod-Shift-Tab  Move focus to the previous window",
    "mod-j          Move focus to the next window",
    "mod-k          Move focus to the previous window",
    "mod-m          Move focus to the master window",
    "",
    "-- modifying the window order",
    "mod-Return   Swap the focused window and the master window",
    "mod-Shift-j  Swap the focused window with the next window",
    "mod-Shift-k  Swap the focused window with the previous window",
    "",
    "-- resizing the master/slave ratio",
    "mod-h  Shrink the master area",
    "mod-l  Expand the master area",
    "",
    "-- floating layer support",
    "mod-t  Push window back into tiling; unfloat and re-tile it",
    "",
    "-- increase or decrease number of windows in the master area",
    "",
    "-- quit, or restart",
    "mod-Shift-q  Quit xmonad",
    "mod-q        Restart xmonad",
    "mod-[1..9]   Switch to workSpace N",
    "",
    "-- Workspaces & screens",
    "mod-Shift-[1..9]   Move client to workspace N",
    "mod-{w,e,r}        Switch to physical/Xinerama screens 1, 2, or 3",
    "mod-Shift-{w,e,r}  Move client to screen 1, 2, or 3",
    "",
    "-- Mouse bindings: default actions bound to mouse events",
    "mod-button1  Set the window to floating mode and move by dragging",
    "mod-button2  Raise the window to the top of the stack",
    "mod-button3  Set the window to floating mode and resize by dragging"]
