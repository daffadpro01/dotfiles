Config { 

   -- appearance
     font =         "xft:Ubuntu:weight=bold:size=13:antialias=true:hinting=true"
   , additionalFonts= ["xft:Font Awesome 5 Free-Solid:style=solid:size=12"
		       ,"xft:Ubuntu Mono:size=12"
                       ,"xft:Source Code Pro:size=12"
		      ] 
   , bgColor =      "#282a36"
   , fgColor =      "#f8f8f2"
   , position =     Static { xpos = 0, ypos = 0, width = 1920, height = 35}
   , border =       BottomB
   , borderColor =  "#2E3440"

   -- layout
   , sepChar =  "%"   -- delineator between plugin names and straight text
   , alignSep = "}{"  -- separator between left-right alignment
   , template = " %UnsafeStdinReader%}{%cpu% <fn=2>|</fn> %memory% <fn=2>|</fn> %dynnetwork% <fn=2>|</fn> %battery% <fn=2>|</fn> %uptime% <fn=2>|</fn> %date% "

   -- general behavior
   , lowerOnStart =     True    -- send to bottom of window stack on start
   , hideOnStart =      False   -- start with window unmapped (hidden)
   , allDesktops =      True    -- show on all desktops
   , overrideRedirect = True    -- set the Override Redirect flag (Xlib)
   , pickBroadest =     False   -- choose widest display (multi-monitor)
   , persistent =       True    -- enable/disable hiding (True = disabled)
   , iconRoot = "~/xmobaricons/"
   

   , commands = 

        -- weather monitor
        [ Run Weather "RJTT" [ "--template", "<skyCondition> | <fc=#4682B4><tempC></fc>°C | <fc=#4682B4><rh></fc>% | <fc=#4682B4><pressure></fc>hPa"
                             ] 36000
	, Run UnsafeStdinReader

        -- network activity monitor (dynamic interface resolution)
        , Run DynNetwork     [ "--template" , "<fn=1></fn> <fn=3> </fn><tx>kB/s <fn=2>|</fn> <rx>kB/s"
                             , "--Low"      , "1000"       -- units: B/s
                             , "--High"     , "5000"       -- units: B/s
                             , "--low"      , "#50fa7b"
                             , "--normal"   , "#ffb86c"
                             , "--high"     , "#ff5555"
                             ] 10

        -- cpu activity monitor
        , Run Cpu       [ "--template" , "<fn=1></fn> <fn=3> </fn><total>%"
                             , "--Low"      , "50"         -- units: %
                             , "--High"     , "85"         -- units: %
                             , "--low"      , "#50fa7b"
                             , "--normal"   , "#ffb86c"
                             , "--high"     , "#ff5555"
                             ] 10
                          
        -- memory usage monitor
        , Run Memory         [ "--template" ,"<fn=1></fn> <fn=3> </fn><usedratio>%"
                             , "--Low"      , "20"        -- units: %
                             , "--High"     , "90"        -- units: %
                             , "--low"      , "#50fa7b"
                             , "--normal"   , "#ffb86c"
                             , "--high"     , "#ff5555"
                             ] 10

        -- battery monitor
        , Run Battery        [ "--template" , "<fn=1></fn> <fn=3> </fn><acstatus>"
                             , "--Low"      , "10"        -- units: %
                             , "--High"     , "80"        -- units: %
                             , "--low"      , "#50fa7b"
                             , "--normal"   , "#ffb86c"
                             , "--high"     , "#ff5555"

                             , "--" -- battery specific options
                                       -- discharging status
                                       , "-o"	, "<left>% (<timeleft>)"
                                       -- AC "on" status
                                       , "-O"	, "<fc=#EBCB8B>Charging</fc>"
                                       -- charged status
                                       , "-i"	, "<fc=#A3BE8C>Charged</fc>"
                             ] 50

	, Run Uptime ["--template" , "<fn=1></fn> <fn=3> </fn><hours>h <minutes>m <seconds>s"] 10

        -- time and date indicator 
        --   (%F = y-m-d date, %a = day of week, %T = h:m:s time)
        , Run Date           "<fn=1></fn> <fn=3> </fn>%a %F %T" "date" 10

        ]
   }
